## Instruções do projeto

### Passo 1
Instale os módulos usando
`npm install` ou `yarn install`

### Passo 2
Inicie o projeto usando
`npm start` ou `yarn start` e 

### Passo 3
Veja o projeto no endereço 
`http://localhost:3000/`
