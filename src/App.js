import React from 'react';

// Components
import Header from './components/Header'
import TransactionForm from './components/TransactionForm'
import TransactionList from './components/TransactionList'

const initialState = {
	transactions: [],
	total: 0
}

class App extends React.Component {
  constructor(props) {
    super(props)

		if(localStorage.getItem('transactions')) {
			this.state = {
				transactions: JSON.parse(localStorage.getItem('transactions')),
				total: localStorage.getItem('total')
			}
		} else {
			this.state = {
				...initialState
			}
		}

		this.handleForm = this.handleForm.bind(this)
	}
	
	handleForm(data) {
		// Check if is + or -
		data.valor = (data.tipo === 'Compra') ? -data.valor : data.valor
		
		// Compute total
		let transactionsSum = this.state.transactions.reduce((a,s) => {
			return a + s.valor;
		}, 0)
		let total = transactionsSum + data.valor

		// Update State
		this.setState({
			transactions: [...this.state.transactions, data],
			total: total
		})
	}

  render() {
    return (
      <div className="App">
        <Header/>

				<main className="Main">
					<TransactionForm handleForm={this.handleForm} />
					<TransactionList {...this.state}/>
				</main>
      </div>
    );
  }
}

export default App;
