import React from 'react'

// Assets
import Logo from '../assets/images/logo.svg'

class Header extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			navigationIsActive: false
		}
	}

	toggleNavigation() {
		this.setState({
			navigationIsActive: !this.state.navigationIsActive
		})
	}
	
	render() {
		const {navigationIsActive} = this.state

		return (
			<header className="Header">
				<div className="Header__inner">
					<div className="Header__logo">
						<img width="34" src={Logo} alt="Logo" />
					</div>

					<div className="Header__name">
						Controle financeiro
					</div>
					
					<div className="Header__menu">
						<nav className={`Navigation ${navigationIsActive ? `Navigation__active` : ''}`}>
							<div className="Navigation__toggle" onClick={this.toggleNavigation.bind(this)}>
								<span></span>
								<span></span>
								<span></span>
							</div>
							<ul className="Navigation__menu">
								<li><a href="/">Resumo</a></li>
								<li><a href="/">Dashboard</a></li>
								<li><a href="/">Configurações</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
		)
	}
}

export default Header
