import React from 'react'

const initialState = {
	tipo: 'Compra',
	mercadoria: '',
	valor: ''
}

class TransactionForm extends React.Component {
	constructor(props) {
		super(props)

		this.handleChange = this.handleChange.bind(this)
		this.handleValue = this.handleValue.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)

		this.state = {
			...initialState
		}
	}

	handleChange(e) {
		const {name, value} = e.target

		this.setState({
			[name]: value
		})
	}

	handleValue(e) {
		this.setState({
			valor: parseInt(e.target.value)
		})
	}
	
	handleSubmit(e) {
		e.preventDefault()

		this.setState(initialState)

		this.props.handleForm(this.state)
	}

	render() {
		const enabled = this.state.mercadoria && this.state.valor

		return (
			<div className="TransactionForm">
				<h2>Nova transação</h2>
				<form className="Form" onSubmit={this.handleSubmit}>
					<div className="Form__field">
						<label>Tipo de transação</label>
						<select value={this.state.tipo} name="tipo" onChange={this.handleChange}>
							<option value="Compra">Compra</option>
							<option value="Venda">Venda</option>
						</select>
					</div>
	
					<div className="Form__field">
						<label>Nome da mercadoria</label>
						<input 
							value={this.state.mercadoria} 
							name="mercadoria" 
							type="text" 
							placeholder="" 
							onChange={this.handleChange} 
							data-prefix="R$ "/>
					</div>
	
					<div className="Form__field">
						<label>Valor</label>
						<input 
							value={this.state.valor} 
							name="valor" 
							type="number" 
							placeholder="R$ 0,00" 
							onChange={this.handleValue} 
							inputMode="numeric"/>
					</div>
	
					<div className="Form__end">
						<button 
							type="submit" 
							className="Button Button--block" 
							disabled={!enabled}>
							Adicionar transação
						</button>
					</div>
				</form>
			</div>
		)
	}
}

export default TransactionForm
