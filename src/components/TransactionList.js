import React from 'react'

class TransactionList extends React.Component {
	componentDidUpdate() {
		localStorage.setItem('total', this.props.total)
		localStorage.setItem('transactions', JSON.stringify(this.props.transactions))
	}

	render() {
		const {transactions, total} = this.props
		
		return (
			<div className="TransactionList">
				{this.props.transactions.length > 0 &&
					<div>
						<h2>Extrato de transações</h2>
						<table className="Table">
							<thead>
								<tr>
									<th>Mercadoria</th>
									<th>Valor</th>
								</tr>
							</thead>
			
							<tbody>
								{transactions.map((item, index) => {
									return (
										<tr key={index} className={`Table__row Table__row--${item.tipo === 'Venda' ? 'add' : 'rem'}`}>
											<td>
												<span className={`Icon Icon--${item.tipo === 'Venda' ? 'add' : 'rem'}`}></span>
												{item.mercadoria}
											</td>
											<td>
												R$ {Math.abs(item.valor)}
											</td>
										</tr>
									)
								})}
							</tbody>

							<tfoot>
								<tr className={`Table__row Table__row--${total > 0 ? 'add' : 'rem'}`}>
									<td>Total</td>
									<td>
										R$ {Math.abs(total)}
										<small>{(total > 0) ? '(lucro)' : '(prejuízo)'}</small>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				}
			</div>
		)
	}
}

export default TransactionList
