import React from 'react'
import ReactDOM from 'react-dom'

// Components
import App from './App'

// Styles
import './styles/styles.scss'

// ServiceWorker
import * as serviceWorker from './serviceWorker'

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.unregister()
